function grandParent(){
    let y = 100
    return function parent(){
        let x = 10;
        return function child(){
            x+=1
            console.log(x,y)
        }
    } 
}

var adopted = grandParent()
adopted();