    //check for prime number with for loop, while and do while
    //logic num%2==0 flage = 1 else prime

let number = 13
var flage = 0
for (let index = 2; index < number; index++) {

        if(number%index == 0){
            flage = 1;
        }    
}
if(!flage){
    console.log("Prime Number");
}

//With While Loop
flage = 0;
let counter =  2;
while(counter < number){
    if(number % counter == 0){
        flage = 1;
        break;
    }
    counter++;
}
if(!flage){
    console.log("Prime Number");
}

//do while

counter = 2
flage = 0
    do{
        if(number % counter == 0){
            flage = 1;
            break;
        }
        counter++;       
    }while(counter < number)

    if(!flage){
        console.log("Prime Number");
    }