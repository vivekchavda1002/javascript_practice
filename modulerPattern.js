let myModule = (function () {
    'use strict';
            let _myName = "Mike"
            let publicName = "Jake"
            function _getMyName() {
                return _myName;                
            }
            return {
                get_name:function() {
                    return _getMyName();                    
                },
                publicName:publicName
            }
})();
console.log(myModule.get_name());
console.log(myModule.publicName);
console.log(myModule);
// myModule._getMyName();