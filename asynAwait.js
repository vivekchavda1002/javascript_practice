async function getData(){
    console.log("Inside getData");
    let response = await fetch('https://api.publicapis.org/entries');
    console.log("after API function");
    let userList = await response.json()
    console.log("After userList");
    return userList
}
console.log("Before calling data");
let result =  getData();
console.log("After calling the data");
result.then(data=>console.log(data))
console.log(result);
