class parent{
    constructor(name) {
        this.name = name;
        console.log("Class Parent");
    }
    greet(){
        console.log(`Hello Mr. ${this.name}`);
    }
}
class child extends parent{
    constructor(name){
        console.log("Class Child");
        super(name)
    }
}

let object =  new child("Micle");

object.greet();