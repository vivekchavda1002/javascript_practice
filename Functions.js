function calculate(num){
return num*10
}
console.log(calculate(10))

//arrow
  sum =  ()=>{
      console.log("hello")
      console.log(sum())
  }


  //one-line

  let fun = new Function("x","y","return 10*20");

  console.log(fun(10,20))

  //scope

  var foo  = "bar"

  function bar(){
    var foo = "baz"
  }
  function baz()
  {
    bam = "yay" // it will create this variable on globle scope
  }
  baz()
  console.log(bam)